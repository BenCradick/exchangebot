require("dotenv").config({ path: "../.env" });
("use strict");

const RippleAPI = require("ripple-lib").RippleAPI;

const api = new RippleAPI({
  server: "wss://s1.ripple.com" // Public rippled server
});

main();

function main(y) {
  const myAddress = process.env.XRP_WALLET;
  const address = "r9cZA1mLK5R5Am25ArfXFmqgNwjZgnfk59";
  const currency = {
    base: {
      currency: "QAU",
      counterparty: process.env.QAU_GATE_ADDR
    },
    counter: {
      currency: "XRP"
    }
  };

  const options = {
    limit: 20
  };
  switch (y) {
    case "asks":
      if (!api.isConnected()) {
        return api.connect().then(setTimeout(main, 3000, "asks"));
      }
      return api.getBalances(myAddress).then(balance => {
        let balanceQAU;
        for (let x in balance) {
          if (balance[x].currency === "QAU") {
            balanceQAU = balance[x].value;
          }
        }

        //console.log('getting account balance for', myAddress);
        return api.getOrderbook(address, currency, options).then(orderbook => {
          let start = 0;
          greaterThan(balanceQAU, 50)
            ? isFirstChain(orderbook, balance, myAddress, start, "asks")
            : setTimeout(main, 3000, "asks");
        });
      });

      break;
    case "bids":
      if (!api.isConnected()) {
        return api.connect().then(setTimeout(main, 3000, "bids"));
      }
      return api.getBalances(myAddress).then(balance => {
        let balanceXRP, balanceQAU;
        for (let x in balance) {
          if (balance[x].currency === "XRP") {
            balanceXRP = balance[x].value;
          } else if (balance[x].currency === "QAU") {
            balanceQAU = balance[x].value;
          }
        }

        //console.log('getting account balance for', myAddress);
        return api.getOrderbook(address, currency, options).then(orderbook => {
          let start = 0;
          greaterThan(balanceXRP, 50)
            ? isFirstChain(orderbook, balance, myAddress, start, "bids")
            : setTimeout(main, 3000, "bids");
        });
      });

    default:
      api.connect().then(() => {
        return api.getBalances(myAddress).then(balance => {
          console.log(balance);
          let balanceXRP, balanceQAU;
          for (let x in balance) {
            if (balance[x].currency === "XRP") {
              balanceXRP = balance[x].value;
            } else if (balance[x].currency === "QAU") {
              balanceQAU = balance[x].value;
            }
          }

          //console.log('getting account balance for', myAddress);
          return api
            .getOrderbook(address, currency, options)
            .then(orderbook => {
              for (var i = 0; i < 10; i++) {
                console.log(
                  orderbook.asks[i].properties.maker +
                    " " +
                    orderbook.asks[i].properties.makerExchangeRate +
                    " " +
                    orderbook.asks[i].specification.quantity.value +
                    "\t" +
                    orderbook.bids[i].properties.maker +
                    " " +
                    orderbook.bids[i].properties.makerExchangeRate +
                    " " +
                    orderbook.bids[i].specification.quantity.value
                );
              }
              let start = 0;

              greaterThan(balanceQAU, 50)
                ? isFirstChain(orderbook, balance, myAddress, start, "asks")
                : setTimeout(main, 10000, "asks");
              greaterThan(balanceXRP, 50)
                ? isFirstChain(orderbook, balance, myAddress, start, "bids")
                : setTimeout(main, 10000, "bids");
            });
        });
      });
  }
}
/**
 *
 * @param orderbook the JSON object that contains the orderbook
 * @param side The orderbook array you want to target
 * @param quantThreshold
 * @param percentThreshold
 * @param i where to start comparison
 * @param k opposite array comparison location
 * @returns {float}
 */

function getValToBeat(orderbook, side, quantThreshold, percentThreshold, i, k) {
  let sum = 0;
  let j = i;
  let target, originalQT, offerRate, baseRate;

  switch (side) {
    case "asks":
      target =
        1 /
        orderbook.bids[k].properties.makerExchangeRate *
        (1 + percentThreshold);
      originalQT = quantThreshold;
      quantThreshold /= parseFloat(
        orderbook.asks[i].properties.makerExchangeRate
      );
      while (sum <= quantThreshold) {
        if (orderbook.asks[j].properties.maker !== process.env.XRP_WALLET) {
          sum += parseFloat(orderbook.asks[j].specification.quantity.value);
        }
        ++j;
      }
      offerRate = parseFloat(orderbook.asks[j].properties.makerExchangeRate);
      baseRate = parseFloat(orderbook.asks[i].properties.makerExchangeRate);
      //if the offer i'm looking to beat is my own, restart loop. This is to avoid getting in bidding war with self.
      if (target <= offerRate && offerRate - baseRate < 0.002) {
        // if(orderbook.asks[i].properties.maker === process.env.XRP_WALLET){
        //     return setTimeout(main, 10000, side);
        // }
        return baseRate;
      } else {
        i++;
        return getValToBeat(
          orderbook,
          side,
          originalQT,
          percentThreshold,
          i,
          k
        );
      }

      break;

    case "bids":
      target =
        parseFloat(orderbook.asks[k].properties.makerExchangeRate) *
        (1 + percentThreshold);
      originalQT = quantThreshold;
      while (sum <= quantThreshold) {
        if (orderbook.bids[j].properties.maker !== process.env.XRP_WALLET) {
          sum += parseFloat(orderbook.bids[j].specification.quantity.value);
        }
        ++j;
      }
      offerRate = parseFloat(orderbook.bids[j].properties.makerExchangeRate);
      baseRate = parseFloat(orderbook.bids[i].properties.makerExchangeRate);
      if (target <= offerRate && offerRate - baseRate < 0.02) {
        // if(orderbook.bids[i].properties.maker === process.env.XRP_WALLET){
        //     return setTimeout(main, 10000, side);
        // }
        return baseRate;
      } else {
        i++;
        return getValToBeat(
          orderbook,
          side,
          originalQT,
          percentThreshold,
          i,
          k
        );
      }
      break;

    default:
      throw "Side not specified";
      break;
  }
}
function getSigPosBids(orderbook, quantThreshold) {
  let sum = 0;
  let i = 0;
  while (sum < quantThreshold) {
    if (orderbook.bids[i].properties.maker !== process.env.XRP_WALLET) {
      sum += +orderbook.bids[i].specification.quantity.value;
    }
    ++i;
  }
  return i;
}
function getSigPosAsks(orderbook, quantThreshold) {
  let sum = 0;
  let i = 0;
  while (sum < quantThreshold) {
    if (orderbook.asks[i].properties.maker !== process.env.XRP_WALLET) {
      sum += +orderbook.asks[i].specification.quantity.value;
    }
    ++i;
  }
  return i;
}

function isFirstChain(orderbook, balance, myAddress, start, side) {
  // if(isFirst(myAddress, orderbook, side)){
  //     ++start;
  // }

  placeBid(orderbook, balance, myAddress, start, side);
}
function isFirst(myAddress, orderbook, side) {
  return myAddress === pickSide(orderbook, side);
}
function pickSide(orderbook, side) {
  if (side === "asks") {
    return orderbook.asks;
  } else if (side === "bids") {
    return orderbook.bids;
  }
}

function placeBid(orderbook, balance, myAddress, start, side) {
  let k;
  if (side === "asks") {
    k = getSigPosAsks(orderbook, 400);
  } else {
    k = getSigPosBids(orderbook, 100);
  }

  let bidVal = getValToBeat(orderbook, side, 200, 0.01, start, k) - 0.000000001;

  if (isNaN(bidVal)) {
    return;
  }
  let orderbookSide = pickSide(orderbook, side);
  let a = 0;
  while (
    myAddress !== orderbookSide[a].properties.maker &&
    a < orderbookSide.length - 1
  ) {
    ++a;
  }

  var payment;
  let currency = 1;
  if (side === "bids") {
    currency = 0;
  }
  var bid = +balance[currency].value;
  //to keep minimum wallet balance kosher
  if (currency === 0) {
    bid -= 45;
  }
  payment = bid * bidVal;
  if (
    Math.abs(orderbookSide[a].properties.makerExchangeRate - bidVal) <
      0.00000001 &&
    Math.abs(bid - orderbookSide[a].specification.quantity.value) < 0.1
  ) {
    return setTimeout(main, 10000, side);
  }
  return api
    .getOrders(myAddress, { limit: 10 })
    .then(orders => {
      let makerCurrency, makerAddr, takerCurrency, takerAddr;
      //sets the currency ID to the correct value depending on which side of the spread order will target also applies qua address
      // to appropriate side so that  order gets built correctly.
      if (side === "asks") {
        makerCurrency = "QAU";
        takerCurrency = "XRP";
        makerAddr = process.env.QAU_GATE_ADDR;
      } else {
        makerCurrency = "XRP";
        takerCurrency = "QAU";
        takerAddr = process.env.QAU_GATE_ADDR;
      }

      let sequence;
      for (let i = 0; i < orders.length; i++) {
        if (orders[i].specification.totalPrice.currency === takerCurrency) {
          sequence = orders[i].properties.sequence;
        }
      }
      let order = buildOrder(
        buildDeliverable(makerCurrency, bid.toPrecision(16), makerAddr),
        buildDeliverable(takerCurrency, payment.toPrecision(16), takerAddr),
        sequence
      );
      return api.prepareOrder(myAddress, order).then(order => {
        let signedJSON = api.sign(order.txJSON, process.env.KEY);

        return api.submit(signedJSON.signedTransaction).then(response => {
          console.log(response);
          return setTimeout(main, 10000, side);
        });
      });
    })
    .catch(console.error());
}
function buildOrder(quantity, price, toBeReplaced) {
  let order = new Object();
  order.quantity = quantity;
  order.totalPrice = price;
  if (typeof toBeReplaced !== "undefined") {
    order.orderToReplace = toBeReplaced;
  }
  return defined(toBeReplaced)
    ? {
        direction: "sell",
        quantity: quantity,
        totalPrice: price,
        orderToReplace: toBeReplaced
      }
    : { direction: "sell", quantity: quantity, totalPrice: price };
}
function buildDeliverable(currency, amount, address) {
  return currency !== "XRP "
    ? { currency: currency, counterparty: address, value: amount.toString() }
    : { currency: currency, value: amount.toString() };
}
function defined(n) {
  return typeof n !== "undefined";
}

function greaterThan(value, test) {
  return value > test;
}
