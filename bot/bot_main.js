
("use strict");
require("dotenv").config({ PATH: "../.env" });
module.exports.bot_main = (currencyB) => {

    const currencyA = "XRP";
    const RippleAPI = require("ripple-lib").RippleAPI;

    const api = new RippleAPI({
        server: "wss://s1.ripple.com" // Public rippled server
    });

    const address = getAddress(currencyB);
    const currency = {
        base: {

            currency: currencyB,
            counterparty: getAddress(currencyB)
        },
        counter: {
            currency: currencyA
        }

    };
    const minValA = 50;

    const myAddress = getAddressXrp(currencyB);
    // const options = {
    //     limit: 40
    // };
    api.connect().then(() => {
        return api.getBalances(myAddress).then(balance => {
            console.log(balance);
            let balanceA, balanceB;
            for (let x in balance) {
                if (balance[x].currency === currencyA) {
                    balanceA = balance[x].value;
                } else if (balance[x].currency === currencyB) {
                    balanceB = balance[x].value;
                }
            }

            //console.log('getting account balance for', myAddress);
            return api
                .getOrderbook(address, currency)
                .then(orderbook => {
                    orderbook.asks.sort(function(a,b){
                        return parseFloat(a.properties.makerExchangeRate) - parseFloat(b.properties.makerExchangeRate)
                    });
                    for (var i = 0; i < 10; i++) {
                        console.log(
                            orderbook.asks[i].properties.maker +
                            " " +
                             orderbook.asks[i].properties.makerExchangeRate +
                            " " +
                            orderbook.asks[i].specification.quantity.value +
                            "\t" +
                            orderbook.bids[i].properties.maker +
                            " " +
                            orderbook.bids[i].properties.makerExchangeRate +
                            " " +
                            orderbook.bids[i].specification.quantity.value
                        );
                    }
                    let start = 0;
                    let minValB = getMinValB(orderbook)

                    greaterThan(balanceB, minValB)
                        ? placeBid(orderbook, balance, myAddress, start, "asks")
                        : setTimeout(main, 15000, "asks");
                    greaterThan(balanceA, minValA)
                        ? placeBid(orderbook, balance, myAddress, start, "bids")
                        : setTimeout(main, 15000, "bids");
                });
        });
    });


    function main(y) {
        switch (y) {
            case "asks":
                if (!api.isConnected()) {
                    return api.connect().then(setTimeout(main, 3000, "asks"));
                }
                return api.getBalances(myAddress).then(balance => {
                    let balanceB;
                    for (let x in balance) {
                        if (balance[x].currency === currencyB) {
                            balanceB = balance[x].value;
                        }
                    }

                    //console.log('getting account balance for', myAddress);
                    return api.getOrderbook(address, currency).then(orderbook => {
                        let start = 0;
                        orderbook.asks.sort(function(a,b){
                            return parseFloat(a.properties.makerExchangeRate) - parseFloat(b.properties.makerExchangeRate)
                        });
                        orderbook.bids.sort(function(a,b){
                            return parseFloat(a.properties.makerExchangeRate) - parseFloat(b.properties.makerExchangeRate)
                        });
                        let minValB = getMinValB(orderbook);
                        greaterThan(balanceB, minValB)
                            ? placeBid(orderbook, balance, myAddress, start, "asks")
                            : setTimeout(main, 30000, "asks");
                    });
                });

                break;
            case "bids":
                if (!api.isConnected()) {
                    return api.connect().then(setTimeout(main, 3000, "bids"));
                }
                return api.getBalances(myAddress).then(balance => {
                    let balanceA, balanceB;
                    for (let x in balance) {
                        if (balance[x].currency === currencyA) {
                            balanceA = balance[x].value;
                        } else if (balance[x].currency === currencyB) {
                            balanceB = balance[x].value;
                        }
                    }

                    //console.log('getting account balance for', myAddress);
                    return api.getOrderbook(address, currency).then(orderbook => {
                        let start = 0;
                        orderbook.asks.sort(function(a,b){
                            return parseFloat(a.properties.makerExchangeRate) - parseFloat(b.properties.makerExchangeRate)
                        });
                        orderbook.bids.sort(function(a,b){
                            return parseFloat(a.properties.makerExchangeRate) - parseFloat(b.properties.makerExchangeRate)
                        });
                        greaterThan(balanceA, minValA)
                            ? placeBid(orderbook, balance, myAddress, start, "bids")
                            : setTimeout(main, 3000, "bids");
                    });
                });

           //default:

        }
    }

    /**
     *
     * @param orderbook the JSON object that contains the orderbook
     * @param side The orderbook array you want to target
     * @param bThresh
     * @param percentThreshold
     * @param i where to start comparison
     * @param k opposite array comparison location
     * @returns {float}
     */

    function getValToBeat(orderbook, side, bThresh, percentThreshold, i, k) {
        let sum = 0;
        let j = i - 1;
        let target, originalQT, offerRate, baseRate;

        switch (side) {
            case "asks":
                target =
                    1 /
                    orderbook.bids[k].properties.makerExchangeRate *
                    (1 + percentThreshold);
                originalQT = bThresh;
                // bThresh /= parseFloat(
                //     orderbook.asks[i].properties.makerExchangeRate
                // );
                console.log("bThresh: %d", bThresh);
                while (sum <= bThresh) {
                    ++j;
                    console.log("j: %d \t sum: %d \t bthresh: %d", j, sum, bThresh);
                    if (orderbook.asks[j].properties.maker !== myAddress) {
                        sum += parseFloat(orderbook.asks[j].specification.quantity.value);
                    }
                }
                offerRate = parseFloat(orderbook.asks[j].properties.makerExchangeRate);
                baseRate = parseFloat(orderbook.asks[i].properties.makerExchangeRate);
                //if the offer i'm looking to beat is my own, restart loop. This is to avoid getting in bidding war with self.
                var baseOfferDif =  offerRate - baseRate;
                var pointOnePercentOfBase = baseRate*0.001;
                if (target <= offerRate && baseOfferDif < pointOnePercentOfBase) {
                    return i;
                } else {
                    i++;
                    return getValToBeat(
                        orderbook,
                        side,
                        originalQT,
                        percentThreshold,
                        i,
                        k
                    );
                }

                break;

            case "bids":
                target =
                    parseFloat(orderbook.asks[k].properties.makerExchangeRate) *
                    (1 + percentThreshold);
                originalQT = bThresh;
                while (sum <= bThresh) {
                    ++j;
                    if (orderbook.bids[j].properties.maker !== myAddress) {
                        sum += parseFloat(orderbook.bids[j].specification.quantity.value);
                    }
                }
                offerRate = parseFloat(orderbook.bids[j].properties.makerExchangeRate);
                baseRate = parseFloat(orderbook.bids[i].properties.makerExchangeRate);
                baseOfferDif =  offerRate - baseRate;
                pointOnePercentOfBase = baseRate*0.001;
                if (1 / target <= offerRate && baseOfferDif < pointOnePercentOfBase) {
                    return i;
                } else {
                    i++;
                    return getValToBeat(
                        orderbook,
                        side,
                        originalQT,
                        percentThreshold,
                        i,
                        k
                    );
                }
                break;

            default:
                throw "Side not specified";
                break;
        }
    }

    function getSigPosBids(orderbook, bThresh) {
        let sum = 0;
        let i = 0;
        while (sum < bThresh) {
            if (orderbook.bids[i].properties.maker !== myAddress) {
                sum += +orderbook.bids[i].specification.quantity.value;
            }
            ++i;
        }
        return i;
    }

    function getSigPosAsks(orderbook, bThresh) {
        let sum = 0;
        let i = 0;
        while (sum < bThresh) {
            if (orderbook.asks[i].properties.maker !== myAddress) {
                sum += +orderbook.asks[i].specification.quantity.value;
            }
            ++i;
        }
        return i;
    }

    function placeBid(orderbook, balance, myAddress, start, side) {
        let k;
        let bThresh = calcBThresh(orderbook);
        if (side === "asks") {

            k = getSigPosAsks(orderbook, bThresh);
        } else {
            k = getSigPosBids(orderbook, bThresh);
           // bThresh = 100;

        }

        let locToBeat = getValToBeat(orderbook, side, bThresh, 0.011, start, k);

        if (isNaN(locToBeat)) {
            return;
        }

        let orderbookSide = pickSide(orderbook, side);
        let b;
        let loc;
        let inPos = false;
        let correctVal = false;
        for (b = 0; b <= locToBeat; b++) {
            if (orderbookSide[b].properties.maker === myAddress) {
                inPos = true;
                loc = b;
                break;
            }
        }

        let decrementor = 0.0000001;
        let bidVal =
            parseFloat(orderbookSide[locToBeat].properties.makerExchangeRate) -
            decrementor;

        let currentBidLocation = 0;
        while (
            myAddress !== orderbookSide[currentBidLocation].properties.maker &&
            currentBidLocation < orderbookSide.length - 1
            ) {
            ++currentBidLocation;
        }


        let payment;
        let currency = 1;
        if (side === "bids") {
            currency = 0;
        }
        let bid = +balance[currency].value;
        //to keep minimum wallet balance kosher xrp is represented by 0, anything else is one
        if (currency === 0) {
            bid -= 45;
        }
        payment = bid * bidVal;
        if (loc !== undefined) {
            if (side === "asks") {
                if (Math.abs(bid - orderbookSide[loc].specification.quantity.value) < 1) {
                    correctVal = true;
                }
            } else {
                if (Math.abs(bid - orderbookSide[loc].specification.totalPrice.value) < 1) {
                    correctVal = true;
                }
            }
        }
        if (inPos && correctVal && currentBidCorrectVal(bidVal, orderbookSide, currentBidLocation)) {
            return setTimeout(main, 15000, side);
        }
        return api
            .getOrders(myAddress, {limit: 10})
            .then(orders => {
                let makerCurrency, makerAddr, takerCurrency, takerAddr;
                //sets the currency ID to the correct value depending on which side of the spread order will target also applies qua address
                // to appropriate side so that  order gets built correctly.
                if (side === "asks") {
                    makerCurrency = currencyB;
                    takerCurrency = currencyA;
                    makerAddr = address;
                } else {
                    makerCurrency = currencyA;
                    takerCurrency = currencyB;
                    takerAddr = address;
                }

                let sequence;
                for (let i = 0; i < orders.length; i++) {
                    if (orders[i].specification.totalPrice.currency === takerCurrency) {
                        sequence = orders[i].properties.sequence;
                    }
                }
                let order = buildOrder(
                    buildDeliverable(makerCurrency, bid.toPrecision(16), makerAddr),
                    buildDeliverable(takerCurrency, payment.toPrecision(16), takerAddr),
                    sequence
                );
                return api.prepareOrder(myAddress, order).then(order => {

                    let key = getKey(currencyB);
                    let signedJSON = api.sign(order.txJSON, key, {
                        maxFee: 12
                    });
                    return api.submit(signedJSON.signedTransaction).then(response => {
                        orderLog(orderbookSide, side);
                        console.log("process: placeBid");
                        console.log(Date.now());
                        console.log(order.txJSON);
                        console.log(response);
                        if(response.resultCode === "tecUNFUNDED_OFFER"){
                            return unfundedErrorLog(order);

                        }
                        return setTimeout(
                            checkBidPlacement,
                            8000,
                            decrementor,
                            side,
                            balance
                        );
                    });
                });
            })
            .catch();
    }

    function pickSide(orderbook, side) {
        if (side === "asks") {
            return orderbook.asks;
        } else if (side === "bids") {
            return orderbook.bids;
        }
    }
    function getOppositeSide(orderbook, side) {
        if (side === "asks") {
            return orderbook.bids;
        } else if (side === "bids") {
            return orderbook.asks;
        }
    }

    function checkBidPlacement(decrementor, side, balance) {
        return api.getOrderbook(address, currency).then(postOrderBook => {
            return api
                .getOrders(myAddress.toString())
                .then(postOrders => {
                    postOrderBook.asks.sort(function(a,b){
                        return parseFloat(a.properties.makerExchangeRate) - parseFloat(b.properties.makerExchangeRate)
                    });
                    postOrderBook.bids.sort(function(a,b){
                        return parseFloat(a.properties.makerExchangeRate) - parseFloat(b.properties.makerExchangeRate)
                    });
                    let k;
                    let postOrderBookOpposite = getOppositeSide(postOrderBook, side);
                    let bThresh = calcBThresh(postOrderBook);
                    if (side === "asks") {
                        //bThresh = 100 * parseFloat(postOrderBookOpposite[0].properties.makerExchangeRate);
                        console.log(bThresh);
                        k = getSigPosAsks(postOrderBook, bThresh);
                    } else {
                       // bThresh ;
                        k = getSigPosBids(postOrderBook, bThresh);
                    }

                    let newlocToBeat = getValToBeat(postOrderBook, side, bThresh, 0.011, 0, k);
                    //correct location
                    let corLoc = false;
                    let postOrderbookSide = pickSide(postOrderBook, side);
                    if (postOrderbookSide[newlocToBeat].properties.maker === myAddress) {
                        corLoc = true;
                    }
                    if (corLoc ) {
                        orderLog(postOrderbookSide, side);
                        console.log("Process: checkBidPlacement");
                        console.log("decrement value : %d", decrementor);
                        console.log("Bid placed correctly");

                        return setTimeout(main, 15000, side);
                    } else {
                        let makerCurrency, makerAddr, takerCurrency, takerAddr;
                        //sets the currency ID to the correct value depending on which side of the spread order will target also applies qua address
                        // to appropriate side so that  order gets built correctly.
                        if (side === "asks") {
                            makerCurrency = currencyB;
                            takerCurrency = currencyA;
                            makerAddr = address;
                        } else {
                            makerCurrency = currencyA;
                            takerCurrency = currencyB;
                            takerAddr = address;
                        }
                        let payment;
                        let currency = 1;
                        if (side === "bids") {
                            currency = 0;
                        }
                        let bid = +balance[currency].value;
                        //to keep minimum wallet balance kosher
                        if (currency === 0) {
                            bid -= 45;
                        }
                        bidVal =
                            postOrderbookSide[newlocToBeat].properties.makerExchangeRate -
                            decrementor;
                        payment = bid * bidVal;
                        decrementor += 0.0000001;
                        let sequence;
                        for (let i = 0; i < postOrders.length; i++) {
                            if (
                                postOrders[i].specification.totalPrice.currency === takerCurrency
                            ) {
                                sequence = postOrders[i].properties.sequence;
                            }
                        }
                        let order = buildOrder(
                            buildDeliverable(makerCurrency, bid.toPrecision(16), makerAddr),
                            buildDeliverable(takerCurrency, payment.toPrecision(16), takerAddr),
                            sequence
                        );
                        return api.prepareOrder(myAddress, order).then(order => {
                            let key = getKey(currencyB);
                            let signedJSON = api.sign(order.txJSON, key, {
                                maxFee: 12
                            });

                            return api.submit(signedJSON.signedTransaction).then(response => {
                                orderLog(postOrderbookSide, side);
                                console.log("process: checkBidPlacement");
                                console.log("decrementor value: %d", decrementor);
                                console.log("bid placed incorrectly");
                                console.log(Date.now());
                                console.log(order.txJSON);
                                console.log(response);
                                if(response.resultCode === "tecUNFUNDED_OFFER"){
                                    return unfundedErrorLog(order)
                                }

                                return setTimeout(
                                    checkBidPlacement,
                                    8000,
                                    decrementor,
                                    side,
                                    balance
                                );
                            });
                        });
                    }
                });
        });
    }

    function buildOrder(quantity, price, toBeReplaced) {
        let order = new Object();
        order.quantity = quantity;
        order.totalPrice = price;
        if (typeof toBeReplaced !== "undefined") {
            order.orderToReplace = toBeReplaced;
        }
        return defined(toBeReplaced)
            ? {
                direction: "sell",
                quantity: quantity,
                totalPrice: price,
                orderToReplace: toBeReplaced
            }
            : {direction: "sell", quantity: quantity, totalPrice: price};
    }

    function buildDeliverable(currency, amount, address) {
        return currency !== "XRP "
            ? {currency: currency, counterparty: address, value: amount.toString()}
            : {currency: currency, value: amount.toString()};
    }

    function defined(n) {
        return typeof n !== "undefined";
    }

    function greaterThan(value, test) {
        return value > test;
    }

    function orderLog(orderbook, side) {
        console.log("{ orderbook:{ " + side + " { ");
        for (n = 0; n < 10; n++) {
            console.log(
                "{ maker: " +
                orderbook[n].properties.maker +
                ", makerExchangeRate: " +
                orderbook[n].properties.makerExchangeRate +
                ", value: " +
                orderbook[n].specification.quantity.value +
                " },"
            );
        }
        console.log("} } }");
        console.log();
    }
    function getMinValB(orderbook) {
        return minValA * parseFloat(orderbook.bids[0].properties.makerExchangeRate);
    }
    function getAddress(currencyB){
        switch(currencyB){
            case "QAU":
                return process.env.QAU_GATE_ADDR;
                break;
            case "ETH":
                return process.env.ETH_GATE_ADDR;
                break;
            case "ETC":
                return process.env.ETC_GATE_ADDR;
                break;
            default:
                throw error("Invalid currency type")
        }
    }
    function getAddressXrp(currencyB){
        switch(currencyB){
            case "QAU":
                return process.env.XRPQAU_WALLET;
                break;
            case "ETH":
                return process.env.XRPETH_WALLET;
                break;
            case "ETC":
                return process.env.XRPETC_WALLET;
                break;
            default:
                throw error("Invalid currency type")
        }
    }
    function getKey(currencyB){
        switch(currencyB){
            case "QAU":
                return process.env.XRPQAU_KEY;
                break;
            case "ETH":
                return process.env.XRPETH_KEY;
                break;
            case "ETC":
                return process.env.XRPETC_KEY;
                break;
            default:
                throw error("Invalid currency type")
        }
    }
    function unfundedErrorLog(order) {
        let errTX = JSON.parse(order.txJSON);
        console.error("Unfunded Offer \n Currency: %s \n TakerGets (Value): %s \n TakerPays: %s \n",
            errTX.TakerGets.currency, errTX.TakerGets.value, errTX.TakerPays);
        return setTimeout(
            main,
            15000,
            side
        );

    }
    function calcBThresh(orderbook){
       return 5*getMinValB(orderbook);
    }
    function currentBidCorrectVal(bidVal, orderbookSide, currentBidLocation){
        return (Math.abs(1 - (bidVal/parseFloat(orderbookSide[currentBidLocation].properties.makerExchangeRate)))>.05)
    }
};
