require('dotenv').config();
const connectDb = function(){
    const MongoClient = require('mongodb').MongoClient;
    const assert = require('assert');

    // Connection URL
    const url = process.env.db_url;

    // Database Name
    const dbName = process.env.db_name;

    // Use connect method to connect to the server
    MongoClient.connect(url, function (err, client) {
        assert.equal(null, err);
        console.log("Connected successfully to server");

        const db = client.db(dbName);

        client.close();
    });
}
module.exports = connectDb;