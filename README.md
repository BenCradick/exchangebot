# ExchangeBot

## What it does
Takes advantage of high bid-ask spread on relatively low volume cryptocurrency trading pairs by continually placing the best bid or ask price to ensure that all market orders are filled by the bot's orders. This has the effect of growing the position held by the bot's crypto wallets over time and thus increasing the total dollar value of assets under management.

## Why am I proud of *this*?
It is something I wrote that generated a fair amount of money for me, enough that I didn't need to find a part time job during one of my semesters at school. I'm not proud of it because its well written, its not, if you're looking for something that's representative of my current ability to write code check my [GitHub](https://Github.com/BenCradick).
